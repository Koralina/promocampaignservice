package com.promo.demo.customer;

import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CustomerService {

   // private List<Customer> customers = new ArrayList<>();
    private CustomerRepository repository;

    public CustomerService(CustomerRepository repository) {
        this.repository = Objects.requireNonNull(repository, "Customer repository must be defined");
    }
    //Temporary
    //private static int id = 1;

    public void save(Customer customer) {
       // customer.setId(id++);

        //customers.add(customer);
        repository.save(customer);
    }

    public Optional<Customer> getById (Long id){
//        return customers.stream()
//                .filter(customer -> customer.getId() == id)
//                .findFirst();
        return repository.findById(id);
    }

}
