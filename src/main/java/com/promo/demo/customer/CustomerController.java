package com.promo.demo.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

@RestController
public class CustomerController {

    private CustomerService service;

    @Autowired
    public CustomerController(CustomerService service) {
        this.service = Objects.requireNonNull(service, "Customer service must be defined.");
    }

    @PostMapping("/customers")
    public ResponseEntity save(@RequestBody Customer customer) {
        service.save(customer);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

//    @GetMapping("/customers")
//    public ResponseEntity<List<Customer>> getCustomers(Customer customer) {
//        return ResponseEntity.ok(service.)
//    }
}
