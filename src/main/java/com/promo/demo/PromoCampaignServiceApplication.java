package com.promo.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PromoCampaignServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PromoCampaignServiceApplication.class, args);
    }

}
