package com.promo.demo.campaign;

import com.promo.demo.customer.Customer;
import com.promo.demo.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
public class CampaignController {

    private CampaignService service;
    private CustomerService customerService;

    @Autowired
    public CampaignController(CampaignService service, CustomerService customerService) {
        this.service = Objects.requireNonNull(service, "Campaign service must be defined");
        this.customerService = Objects.requireNonNull(customerService, "Customer service must be defined");
    }

    @PostMapping("/campaigns")
    public ResponseEntity save(@RequestBody Campaign campaign) {
        if (isValid(campaign)) {
            service.save(campaign);
            return ResponseEntity.status(201).build();
        } else return ResponseEntity.badRequest().build();
    }

    private boolean isValid(Campaign campaign) {
//        if (campaign.getName() != null
//                && campaign.getBrand() != null
//                && campaign.getStart() != null
//                && campaign.getEnd() != null
//                && campaign.getStart().isBefore(campaign.getEnd()))
//            return true;
//        else return false;
        boolean isEmpty = campaign.getName().isEmpty()
                || campaign.getBrand().isEmpty()
                || Objects.isNull(campaign.getStart())
                || Objects.isNull(campaign.getEnd());
        boolean startBeforeEnd = campaign.getStart().isBefore(campaign.getEnd());
        Optional<Customer> customerOptional = customerService.getById(campaign.getCustomerId());

        if (customerOptional.isPresent()) {
            boolean correctBrand = campaign.getBrand().equals(customerOptional.get().getBrand());//get to metoda z optionala , customer jest optionalem
            return !isEmpty && startBeforeEnd && correctBrand;
        }
        return false;

    }

    @GetMapping("/campaigns/{brand}")
    public ResponseEntity<List<Campaign>> getByBrand(@PathVariable String brand) {
        return ResponseEntity.ok(service.getCampaignsByBrand(brand));
    }
    @GetMapping("/campaigns")
    public ResponseEntity<List<Campaign>> getCampaignByBrandAndOrCustomerId(@RequestParam (required=false)Long customerId,
                                                                  @RequestParam(required=false)String name) {

        if (Objects.nonNull(customerId) && Objects.isNull(name)) {
        return ResponseEntity.ok(service.getCampaignsByCustomerId(customerId));
    }
        else if(Objects.nonNull(name) && Objects.isNull(customerId)){
            return ResponseEntity.ok(service.getByName(name));
        }
        else if (Objects.nonNull(customerId) && Objects.nonNull(name)){
            return ResponseEntity.ok(service.getByCustomerIdAndName(customerId,name))
                    .ok(service.getCampaignsByCustomerId(customerId));
        }
        else {
            return ResponseEntity.ok(service.findAll());
        }
    }

    @GetMapping("campaigns/{id}")
    public ResponseEntity<Campaign> getById(@PathVariable Long id) {
//        Optional<Campaign> campaign = service.getById(id);
//        if (campaign.isPresent()) {
//            return ResponseEntity.ok(campaign.get());
//        }else{
//            return ResponseEntity.notFound().build();
//        }

        return service.getById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
        //return ResponseEntity.ok(service.getById(id));
    }
}