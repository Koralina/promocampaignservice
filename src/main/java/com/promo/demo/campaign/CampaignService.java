package com.promo.demo.campaign;

import org.springframework.stereotype.Service;
import sun.swing.CachedPainter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CampaignService {
    private List<Campaign> campaigns = new ArrayList<>();
    private CampaignRepository repository;


    public CampaignService(CampaignRepository repository) {
        this.repository = Objects.requireNonNull(repository, "Campaign repository must be defined");
    }

    public void save(Campaign campaign) {
        //campaigns.add(campaign);
        repository.save(campaign);
    }
    public Optional<Campaign> getById(Long id) {
    return repository.findById(id);
}
    public List<Campaign> getCampaignsByBrand(String brand) {
        return repository.findByBrand(brand);
//        return campaigns.stream()
//                .filter(promoCampaign -> promoCampaign.getBrand().equals(brand))
//                .filter(campaign -> campaign.getStart().isBefore(LocalDate.now())
//                        && campaign.getEnd().isAfter(LocalDate.now()))
//                .collect(Collectors.toList());
    }

    public List<Campaign> getCampaignsByCustomerId(Long customerId) {
        return repository.findByCustomerId(customerId);
//        return campaigns.stream()
//                .filter(campaign -> campaign.getCustomerId() == customerId)
//                .collect(Collectors.toList());
    }

    public List<Campaign> getByName(String name) {
        return repository.findByName(name);
    }

    public List<Campaign> getByCustomerIdAndName(Long customerId, String name) {
        return repository.findByCustomerIdAndName(customerId, name);
    }
    public List<Campaign> findAll(){
        return repository.findAll();
    }
}