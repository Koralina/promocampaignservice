package com.promo.demo.campaign;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CampaignRepository extends CrudRepository<Campaign, Long> {

    List<Campaign> findAll();

    List<Campaign> findByBrand(String brand);

    List<Campaign> findByCustomerId(Long customerId);

    List<Campaign> findByName(String name);

    List<Campaign> findByCustomerIdAndName(Long customerId, String name);


}
